from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^panel/black/list/$', views.black_list, name="black_list"),
    url(r'^black/list/ip/add/$', views.ip_add, name="ip_add"),
    url(r'^black/list/ip/del/(?P<pk>\d+)/$', views.ip_del, name="ip_del"),
    
]