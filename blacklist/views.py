from django.shortcuts import render, get_object_or_404, redirect
from .models import Blacklist
from main.models import Main
from news.models import News
from cat.models import Cat
from django.utils import timezone
from subcat.models import Subcat
from trending.models import Trending
from django.contrib.auth import authenticate, login, logout
from django.core.files.storage import FileSystemStorage
import random
from random import randint
from django.contrib.auth.models import User, Group, Permission
from manageuser.models import ManageUser
import string
from ipware import get_client_ip
from ip2geotools.databases.noncommercial import DbIpCity


# Create your views here.

#function to show list of blacklist
def black_list(request):

    ip = Blacklist.objects.all()


    return render(request, 'back/black_list.html', {'ip':ip})


#function to add ip addresses
def ip_add(request):

    if request.method == "POST":
        ip = request.POST.get('ip')

        if ip != "" :
            db = Blacklist(ip=ip)
            db.save()


    return redirect('black_list')

#function to delete IPs
def ip_del(request, pk):

    b = Blacklist.objects.filter(pk=pk)
    b.delete()

    return redirect('black_list')
