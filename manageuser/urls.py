from django.conf.urls import url
from . import views

urlpatterns = [

    url(r'^panel/manage/user/list/$', views.manage_user_list, name="manage_user_list"),
    url(r'^manage/user/del/(?P<pk>\d+)/$', views.manageuser_del, name='manageuser_del'),
    url(r'^panel/manage/user/group/show/$', views.manageuser_group_list, name="manageuser_group_list"),
    url(r'^panel/manage/user/group/add/$', views.manageuser_group_add, name="manageuser_group_add"),
    url(r'^panel/manage/user/group/del/(?P<word>.*)/$', views.manageuser_group_del, name="manageuser_group_del"),
    url(r'^panel/manage/users/groups/show/(?P<pk>\d+)/$', views.users_groups, name="users_groups"),
    url(r'^panel/manage/users/addtogroup/(?P<pk>\d+)/$', views.add_users_to_groups, name="add_users_to_groups"),
    url(r'^panel/manage/users/delfromgroup/(?P<pk>\d+)/(?P<name>.*)/$', views.del_users_from_groups, name="del_users_from_groups"),
    url(r'^panel/manage/user/permission/show/$', views.manageuser_permission_show, name="manageuser_permission_show"),
    url(r'^panel/manage/user/permission/del/(?P<name>.*)/$', views.manageuser_permission_del, name="manageuser_permission_del"),
    url(r'^panel/manage/user/permission/add/$', views.manageuser_permission_add, name="manageuser_permission_add"),
    url(r'^panel/manage/users/with/permission/(?P<pk>\d+)$', views.users_with_permission, name="users_with_permission"),
    url(r'^panel/manage/users/permission/del/(?P<pk>\d+)/(?P<name>.*)/$', views.users_permission_del, name="users_permission_del"),
    url(r'^panel/manage/users/permission/assign/(?P<pk>\d+)/$', views.users_permission_assign, name="users_permission_assign"),
    url(r'^panel/manage/users/groups/permission/(?P<name>.*)/$', views.groups_permission, name="groups_permission"),
    url(r'^panel/manage/users/groups/permission/del/(?P<gname>.*)/(?P<name>.*)/$', views.groups_permission_del, name="groups_permission_del"),
    url(r'^panel/manage/users/groups/permission/add/(?P<name>.*)/$', views.groups_permission_add, name="groups_permission_add"),
    url(r'^panel/profile/$', views.user_profile, name='user_profile'),
    
    
]