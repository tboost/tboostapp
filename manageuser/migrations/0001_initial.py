# Generated by Django 3.0.2 on 2020-02-06 01:28

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='ManageUser',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=30)),
                ('txt', models.TextField()),
                ('email', models.EmailField(max_length=254)),
                ('fb', models.CharField(max_length=30)),
                ('tw', models.CharField(max_length=30)),
                ('yt', models.CharField(max_length=30)),
                ('pint', models.CharField(max_length=30)),
                ('gplus', models.CharField(max_length=30)),
            ],
        ),
    ]
