from django.shortcuts import render, get_object_or_404, redirect
from .models import ManageUser
from news.models import News
from main.models import Main
from cat.models import Cat
from django.utils import timezone
from subcat.models import Subcat
from trending.models import Trending
from django.contrib.auth import authenticate, login, logout
from django.core.files.storage import FileSystemStorage
import random
from random import randint
from django.contrib.auth.models import User, Group, Permission
from django.contrib.contenttypes.models import ContentType
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage



#Your function here...
def manage_user_list(request):

    #user check start
    if not request.user.is_authenticated:
        return redirect('mylogin')
    #user check end

    #user access check start
    perm = 0
    for i in request.user.groups.all():
        if i.name == "masteruser": perm = 1
    if perm == 0:
        error = "Access Denied!"
        return render(request, 'back/error.html', {'error':error})
    #user access check end

    manager = ManageUser.objects.all().exclude(txt="admin")

    return render(request, 'back/manageuser_list.html', {'manager':manager})

#function to delete user
def manageuser_del(request,pk):

    #user check start
    if not request.user.is_authenticated:
        return redirect('mylogin')
    #user check end

    #user access check start
    perm = 0
    for i in request.user.groups.all():
        if i.name == "masteruser": perm = 1
    if perm == 0:
        error = "Access Denied!"
        return render(request, 'back/error.html', {'error':error})
    #user access check end

    manager= ManageUser.objects.get(pk=pk)
    b = User.objects.filter(username=manager.txt)
    b.delete()

    manager.delete()

    return redirect(manage_user_list)

#function view users group list
def manageuser_group_list(request):

    #user check start
    if not request.user.is_authenticated:
        return redirect('mylogin')
    #user check end

    #user access check start
    perm = 0
    for i in request.user.groups.all():
        if i.name == "masteruser": perm = 1
    if perm == 0:
        error = "Access Denied!"
        return render(request, 'back/error.html', {'error':error})
    #user access check end

    group = Group.objects.all().exclude(name="masteruser")

    return render(request, 'back/manageuser_group.html', {'group':group})

#function to add new user group
def manageuser_group_add(request):

    #user check start
    if not request.user.is_authenticated:
        return redirect('mylogin')
    #user check end

    perm = 0
    for i in request.user.groups.all():
        if i.name == "masteruser": perm = 1
    if perm == 0:
        error = "Access Denied!"
        return render(request, 'back/error.html', {'error':error})

    if request.method == "POST":
        name = request.POST.get('name')

        if name != "":
            if len(Group.objects.filter(name=name)) == 0 :
                group = Group(name=name)
                group.save()
            else:
                error = f"{name} group already exist!"
                return render(request, 'back/error.html', {'error':error})

    return redirect(manageuser_group_list)

#function to delete user's group
def manageuser_group_del(request, word):

    #user check start
    if not request.user.is_authenticated:
        return redirect('mylogin')
    #user check end

    #user access check start
    perm = 0
    for i in request.user.groups.all():
        if i.name == "masteruser": perm = 1
    if perm == 0:
        error = "Access Denied!"
        return render(request, 'back/error.html', {'error':error})
    #usser access check end

    g = Group.objects.filter(name=word)
    
    g.delete()

    return redirect(manageuser_group_add)

#function to create users groups
def users_groups(request,pk):

    #user check start
    if not request.user.is_authenticated:
        return redirect('mylogin')
    #user check end

    #user access check start
    perm = 0
    for i in request.user.groups.all():
        if i.name == "masteruser": perm = 1
    if perm == 0:
        error = "Access Denied!"
        return render(request, 'back/error.html', {'error':error})
    #user access check end

    # #get the username
    manager = ManageUser.objects.get(pk=pk)
    #print(manager)
    
    #get name of the user
    user = User.objects.get(username=manager.txt)
    #print(user)


    #set empty array varibale
    ugroup = []
    #get all users groups and append it to the empty array variable.
    for i in user.groups.all():
        ugroup.append(i.name)
    #print(ugroup)

    #get all Group objects and render it to template. Use this to display all groups at the select tag
    group = Group.objects.all().exclude(name="masteruser")
    #print(group)

    return render(request, 'back/users_groups.html', {'ugroup': ugroup, 'group': group, 'pk':pk})


# #function to add user to a group
def add_users_to_groups(request, pk):

    #user check start
    if not request.user.is_authenticated:
        return redirect('mylogin')
    #user check end

    #user access check start
    perm = 0
    for i in request.user.groups.all():
        if i.name == "masteruser": perm = 1
    if perm == 0:
        error = "Access Denied!"
        return render(request, 'back/error.html', {'error':error})
    #user access check end

    #get data from form field
    if request.method == "POST" :
        gname = request.POST.get('gname')
        
        if len(Group.objects.filter(name=gname)) == 0 :
            group = Group(name=gname)
            #group = Group.objects.get(name=gname)
            group.save()
        else:
            error = "{} has already been assigned to group!".format(request.user)
            return render(request, 'back/error.html', {'error':error})

        #get the group name
        #group = Group.objects.get(name=gname)
        #group = Group.objects.get_by_natural_key(gname)
        #print(group)

        #find the username
        manager = ManageUser.objects.get(pk=pk)
        user = User.objects.get(username=manager.txt)

        #assign groups to user
        user.groups.add(group)
         

    return redirect('users_groups' , pk=pk)



#function to delete user from a group
def del_users_from_groups(request, pk, name):

    #user check start
    if not request.user.is_authenticated:
        return redirect('mylogin')
    #user check end

    #user access check start
    perm = 0
    for i in request.user.groups.all():
        if i.name == "masteruser": perm = 1
    if perm == 0:
        error = "Access Denied!"
        return render(request, 'back/error.html', {'error':error})
    #user access check end

    #get the group name
    group = Group.objects.get(name=name)
    
    #find the username
    manager = ManageUser.objects.get(pk=pk)
    user = User.objects.get(username=manager.txt)

    #remove user from group
    user.groups.remove(group)
        

       
    return redirect('users_groups' , pk=pk)




#function to show users permission
def manageuser_permission_show(request):

    #user check start
    if not request.user.is_authenticated:
        return redirect('mylogin')
    #user check end

    #user access check start
    perm = 0
    for i in request.user.groups.all():
        if i.name == "masteruser": perm = 1
    if perm == 0:
        error = "Access Denied!"
        return render(request, 'back/error.html', {'error':error})
    #user access check end

    #get the group name
    perms = Permission.objects.all().exclude(name="master_user")
    
    
    return render(request, 'back/manageusers_permission.html', {'perms':perms})


#function to delete users permission
def manageuser_permission_del(request, name):

    #user check start
    if not request.user.is_authenticated:
        return redirect('mylogin')
    #user check end

    #user access check start
    perm = 0
    for i in request.user.groups.all():
        if i.name == "masteruser": perm = 1
    if perm == 0:
        error = "Access Denied!"
        return render(request, 'back/error.html', {'error':error})
    #user access check end

    #get the permission name
    perms = Permission.objects.filter(name=name)
    perms.delete()
    
    return redirect('manageuser_permission_show')



#function to add permission
def manageuser_permission_add(request):

    #user check start
    if not request.user.is_authenticated:
        return redirect('mylogin')
    #user check end

    #user access check start
    perm = 0
    for i in request.user.groups.all():
        if i.name == "masteruser": perm = 1
    if perm == 0:
        error = "Access Denied!"
        return render(request, 'back/error.html', {'error':error})
    #user access check end

    if request.method == "POST":
        name = request.POST.get('name')
        codename = request.POST.get('codename')

        if len(Permission.objects.filter(codename=codename)) == 0 :
            #to get the contenttype of the app you want to give permission
            content_type = ContentType.objects.get(app_label='main', model='main') #Note: use lowercap for the label and name of the app
            permission = Permission.objects.create(codename=codename, name=name, content_type=content_type)
        else:
            error = "permission already exist!"
            return render(request, 'back/error.html', {'error':error})


    return redirect('manageuser_permission_show')


#function to create users groups
def users_with_permission(request,pk):

    #user check start
    if not request.user.is_authenticated:
        return redirect('mylogin')
    #user check end

    #user access check start
    perm = 0
    for i in request.user.groups.all():
        if i.name == "masteruser": perm = 1
    if perm == 0:
        error = "Access Denied!"
        return render(request, 'back/error.html', {'error':error})
    #user access check end

    # #get the username
    manager = ManageUser.objects.get(pk=pk)
    #print(manager)
    
    #get name of the user
    user = User.objects.get(username=manager.txt)
    #print(user)

    #get user with permission
    permission = Permission.objects.filter(user=user)


    #set empty array varibale
    uperms = []
    #get all users groups and append it to the empty array variable.
    for i in permission:
        uperms.append(i.name)
    #print(uperms)

    #get all permissions and sendthem to template
    perms = Permission.objects.all().exclude(name="master_user")

    

    return render(request, 'back/users_with_permission.html', {'uperms': uperms, 'perms':perms, 'pk':pk})



#function to remove user's permission
def users_permission_del(request,pk,name):

    #user check start
    if not request.user.is_authenticated:
        return redirect('mylogin')
    #user check end

    #user access check start
    perm = 0
    for i in request.user.groups.all():
        if i.name == "masteruser": perm = 1
    if perm == 0:
        error = "Access Denied!"
        return render(request, 'back/error.html', {'error':error})
    #user access check end

    # #get the username
    manager = ManageUser.objects.get(pk=pk)
    #print(manager)
    
    #get name of the user
    user = User.objects.get(username=manager.txt)
    #print(user)

    #get user with permission
    permission = Permission.objects.get(name=name)
    user.user_permissions.remove(permission)


    

    return redirect('users_with_permission', pk=pk)


#function to remove user's permission
def users_permission_assign(request,pk):

    #user check start
    if not request.user.is_authenticated:
        return redirect('mylogin')
    #user check end

    #user access check start
    perm = 0
    for i in request.user.groups.all():
        if i.name == "masteruser": perm = 1
    if perm == 0:
        error = "Access Denied!"
        return render(request, 'back/error.html', {'error':error})
    #user access check end

    #get data from the form field
    if request.method == "POST":
        pname = request.POST.get('pname')
            
        #get the username
        manager = ManageUser.objects.get(pk=pk)
        
        #get name of the user
        user = User.objects.get(username=manager.txt)
        
        
        #get user with permission
               
        permission = Permission.objects.get(name=pname)
        
        #assign permissions to user
        user.user_permissions.add(permission)


    return redirect('users_with_permission', pk=pk)



#function to remove user's permission
def groups_permission(request,name):

    #user check start
    if not request.user.is_authenticated:
        return redirect('mylogin')
    #user check end

    #user access check start
    perm = 0
    for i in request.user.groups.all():
        if i.name == "masteruser": perm = 1
    if perm == 0:
        error = "Access Denied!"
        return render(request, 'back/error.html', {'error':error})
    #user access check end

    #get all the groups
    group = Group.objects.get(name=name)
   
    perms = group.permissions.all()

    #get all permissions
    allperms = Permission.objects.all().exclude(name="master_user")
    
        


    return render(request, 'back/groups_permission.html', {'perms':perms, 'name':name, 'allperms':allperms })


#function to remove user's permission
def groups_permission_del(request,gname,name):

    #user check start
    if not request.user.is_authenticated:
        return redirect('mylogin')
    #user check end

    #user access check start
    perm = 0
    for i in request.user.groups.all():
        if i.name == "masteruser": perm = 1
    if perm == 0:
        error = "Access Denied!"
        return render(request, 'back/error.html', {'error':error})
    #user access check end

    #get group name
    group = Group.objects.get(name=gname)

    #get permission name
    perms = Permission.objects.get(name=name)

    #remove group permissions
    group.permissions.remove(perms)

    
    return redirect('groups_permission', name=gname)


#function to remove user's permission
def groups_permission_add(request, name):

    #user check start
    if not request.user.is_authenticated:
        return redirect('mylogin')
    #user check end

    #user access check start
    perm = 0
    for i in request.user.groups.all():
        if i.name == "masteruser": perm = 1
    if perm == 0:
        error = "Access Denied!"
        return render(request, 'back/error.html', {'error':error})
    #user access check end

    if request.method == "POST":
        pname = request.POST.get('pname')

        #get group name
        group = Group.objects.get(name=name)

        #get permission name
        perms = Permission.objects.get(name=pname)

        #remove group permissions
        group.permissions.add(perms)

        
    return redirect('groups_permission', name=name)


#user profile function
def user_profile(request):
    #login check start
    if not request.user.is_authenticated:
        return redirect('mylogin')
    #login check end

    #user access check start
    perm = 0
    for i in request.user.groups.all():
        if i.name == "masteruser": perm = 1
    if perm == 0:
        error = "Access Denied!"
        return render(request, 'back/error.html', {'error':error})
    

    manager = ManageUser.objects.all()
    print(manager)
    
    
    #get name of the user
    user = User.objects.get(username=request.user)
    print(user)
    
    
             
    
    return render(request, 'back/profile.html', {'user':user})
