from __future__ import unicode_literals
from django.db import models

# Create your models here.

class ManageUser(models.Model):
    fname = models.CharField(max_length=30,default='None')
    lname = models.CharField(max_length=30,default='None')
    txt = models.TextField()
    email = models.EmailField(default=None)
    fb = models.CharField(max_length=30)
    tw = models.CharField(max_length=30)
    yt = models.CharField(max_length=30)
    pint = models.CharField(max_length=30)
    gplus = models.CharField(max_length=30)
    ip = models.TextField(default="")
    country = models.TextField(default="")
    
        
    def __str__(self):
        return self.fname + ' ' + self.lname