from django.shortcuts import render, get_object_or_404, redirect
from .models import Newsletter
from news.models import News
from main.models import Main
from django.core.files.storage import FileSystemStorage
from django.utils import timezone
from subcat.models import Subcat
from cat.models import Cat
from trending.models import Trending
from contactform.models import ContactForm
import random
from random import randint
from django.contrib.auth.models import User, Group, Permission
from django.core.mail import send_mail
from django.conf import settings



# Create your views here.

def news_letter(request):

    if request.method == "POST":
        txt = request.POST.get('txt')

        #check if the text contains @ sign
        res = txt.find('@')
        if int(res) != -1:
            db = Newsletter(txt=txt, status=1, date=timezone.now())
            db.save()
            msg = "Thank you for subscribing to our newsletter!"
            return render(request, 'back/msgbox.html', {'msg':msg})
        else:
            try:
                #save the text as number
                int(txt)
                d = Newsletter(txt=txt, status=2, date=timezone.now())
                d.save()
                msg = "Thank you for subscribing to our newsletter!"
                return render(request, 'back/msgbox.html', {'msg':msg})
            except:
                return redirect('home')
        
            msg = "Thank you for subscribing to our newsletter!"
        return render(request, 'front/msgbox.html', {'msg':msg})
                
        
    return redirect('home')


#function to receive emails
def news_letter_emails(request):

    #user check start
    if not request.user.is_authenticated:
        return redirect('mylogin')
    #user check end

    #user permission check start
    perm = 0
    perms = Permission.objects.filter(user=request.user)
    for i in perms:
        if i.codename == "master_user" : perm = 1
    if perm == 0 :
        error = "Access Denied"
        return render(request, 'back/error.html', {'error':error})
    #user permission check end

    emails = Newsletter.objects.filter(status=1)


    return render(request, 'back/news_email.html', {'emails':emails})



#function to receive phone number
def news_letter_phones(request):

    #user check start
    if not request.user.is_authenticated:
        return redirect('mylogin')
    #user check end

    #user permission check start
    perm = 0
    perms = Permission.objects.filter(user=request.user)
    for i in perms:
        if i.codename == "master_user" : perm = 1
    if perm == 0 :
        error = "Access Denied"
        return render(request, 'back/error.html', {'error':error})
    #user permission check end

    phones = Newsletter.objects.filter(status=2)


    return render(request, 'back/news_phone.html', {'phones':phones})



#function to receive phone number
def news_letter_del(request,pk,num):

    #user check start
    if not request.user.is_authenticated:
        return redirect('mylogin')
    #user check end

    #user permission check start
    perm = 0
    perms = Permission.objects.filter(user=request.user)
    for i in perms:
        if i.codename == "master_user" : perm = 1
    if perm == 0 :
        error = "Access Denied"
        return render(request, 'back/error.html', {'error':error})
    #user permission check end

    b = Newsletter.objects.get(pk=pk)
    b.delete()

    if int(num) == 2:
        return redirect('news_letter_phones')

    return redirect('news_letter_emails')


#send email function
def send_email(request):

    if request.method == 'POST':
        txt = request.POST.get('txt')

        #loop through Newsletter to get all emails and append it to the empty array
        a = []
        for i in Newsletter.objects.all():
            a.append(Newsletter.objects.get(pk=i.pk).txt)

        subject = 'message'
        message = txt
        email_from = settings.EMAIL_HOST_USER
        emails = a 
        send_mail(subject,message,email_from,emails)

        msg = "message sent!"
        
        

    return render(request, 'back/news_email.html', {'msg':msg})

    #function to mapp user to checkbox and delete
def checkbox_emaillist(request):
    if request.method == "POST":
        '''
        #to dynamically get the value of each checkbox
        for i in Newsletter.objects.filter(status=1): #this get email that is turn on
            x = request.POST.get(str(i.pk))
            #print(x)

            if str(x) == 'on':
                b = Newsletter.objects.get(pk=i.pk)
                b.delete()
            '''
        
        #get the email check list pk in array/list
        check = request.POST.getlist('checks[]')
        print(check)
        for i in check: #loop to get email id and name
            # b = Newsletter.objects.get(pk=i).txt #this returns email id and name
            b = Newsletter.objects.get(pk=i) #this returns email id/pk. Note that delete() can only apply to pk/id
            b.delete()
    

    return redirect('news_letter_emails')
