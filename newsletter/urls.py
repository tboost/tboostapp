from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^newsletter/add/$', views.news_letter, name='news_letter'),
    url(r'^panel/newsletter/emails/$', views.news_letter_emails, name='news_letter_emails'),
    url(r'^panel/newsletter/phones/$', views.news_letter_phones, name='news_letter_phones'),
    url(r'^panel/newsletter/del/(?P<pk>\d+)/(?P<num>\d+)/$', views.news_letter_del, name="news_letter_del"),
    url(r'^send/newsletter/email/$', views.send_email, name='send_email'),
    url(r'^checkbx/newsletter/email/$', views.checkbox_emaillist, name='checkbox_emaillist'),
]