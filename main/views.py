from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.decorators import login_required
from .models import Main
from news.models import News
from cat.models import Cat
from django.utils import timezone
from subcat.models import Subcat
from trending.models import Trending
from django.contrib.auth import authenticate, login, logout
from django.core.files.storage import FileSystemStorage
import random
from random import randint
from django.contrib.auth.models import User, Group, Permission
from manageuser.models import ManageUser
import string
from ipware import get_client_ip
from ip2geotools.databases.noncommercial import DbIpCity
from zeep import Client
import requests
import json
from django.views.decorators.csrf import csrf_exempt
from bs4 import BeautifulSoup

from rest_framework import viewsets
from main.serializer import NewsSerializer

from django.http import JsonResponse
from newsletter.models import Newsletter


# Create your views here


#create a function for home page
#@csrf_exempt  #using csrf exempt library
def home(request):
    site = get_object_or_404(Main, pk=1)
    # site = Main.objects.get(pk=1)
    news = News.objects.filter(act=1).order_by('-date')
    cat = Cat.objects.filter(act=1)
    subcat = Subcat.objects.filter(act=1)
    lastnews = News.objects.filter(act=1).order_by('-pk')[:3]
    popnews = News.objects.filter(act=1).order_by('-show')[:5]
    popnews1 = News.objects.filter(act=1).order_by('-show')[:4]
    trending = Trending.objects.all().order_by('-pk')[:5]
    lastnews2 = News.objects.filter(act=1).order_by('-pk')[:4]
    #trending random display
    random_object = Trending.objects.all()[randint(0, len(trending) -1)]

    #randomly choose lastnews from the database
    rand_lastnews = News.objects.all()[randint(0, len(lastnews) -1)]

    '''
    #using zeep technique
    client = Client('xxxxxx.wsdl') #ensure you have .wsdl file
    result = client.service.funcname(1,2,3)
    '''

    '''
    #using json
    url = 'xxxxxxxxxxxx'
    data = {'a':"b", 'c':"d"}
    headers = {'content-Type':application/json, 'API_KEY':'xxxxxxxxx'}
    result = requests.post(url, data=json.dumps(data),headers=headers)
    print(result)
    '''

    #using BeautifulSoup (bs4)
    my_html = """

    <title> This is a test title</title>
    <mytag> Test tag</mytag>


    """
    soup = BeautifulSoup(my_html, 'html.parser')
    print(soup.title)
    print(soup.title.string)
    print(soup.mytag)
    print(soup.mytag.string)

    
    return render(request, 'front/home.html', {'site':site, 'news':news, 'cat':cat, 'subcat':subcat, 'lastnews':lastnews, 'popnews':popnews, 'popnews1':popnews1, 'trending':trending, 'random_object':random_object, 'rand_lastnews':rand_lastnews, 'lastnews2':lastnews2})


#create a function for about page
def about(request):
    site = Main.objects.get(pk=1)
    news = News.objects.filter(act=1).order_by('-date')
    cat = Cat.objects.all()
    subcat = Subcat.objects.all()
    lastnews = News.objects.filter(act=1).order_by('-pk')[:3]
    popnews1 = News.objects.filter(act=1).order_by('-show')[:4]
    trending = Trending.objects.all().order_by('-pk')[:5]

    #trending random display
    random_object = Trending.objects.all()[randint(0, len(trending) -1)]

    return render(request, 'front/about.html', {'site':site, 'news':news, 'cat':cat, 'subcat':subcat, 'lastnews':lastnews, 'popnews1':popnews1, 'trending':trending, 'random_object':random_object})

#create a function for contact page
def contact(request):
    site = Main.objects.get(pk=1)
    news = News.objects.filter(act=1).order_by('-date')
    cat = Cat.objects.all()
    subcat = Subcat.objects.all()
    lastnews = News.objects.filter(act=1).order_by('-pk')[:3]
    popnews1 = News.objects.filter(act=1).order_by('-show')[:4]
    trending = Trending.objects.all().order_by('-pk')[:5]

    #trending random display
    random_object = Trending.objects.all()[randint(0, len(trending) -1)]

    return render(request, 'front/contact.html', {'site':site, 'news':news, 'cat':cat, 'subcat':subcat, 'lastnews':lastnews, 'popnews1':popnews1, 'trending':trending, 'random_object':random_object})

#create a function for the backend
def panel(request):
    #user check start
    if not request.user.is_authenticated:
        return redirect('mylogin')
    #user check end

    #user permission check start
    perm = 0
    perms = Permission.objects.filter(user=request.user)
    for i in perms:
        if i.codename == "master_user" : perm = 1

    '''
    #using random string, numbers and symbols
    
    test = ['@', '_', '!', '#', '$', '%']
    rand = ""
    for i in range(5):
        rand = rand + random.choice(string.ascii_letters) + str(random.randint(0,9)) + str(random.choice(test))
    '''

    '''
    #randomly choose news from the database
    count = News.objects.count() #get the length of the news
    rand = News.objects.all()[randint(0,count-1)]
    '''

    '''
    #get user ip
    ip, is_routable = get_client_ip(request)
    if ip is None:
        ip = "0.0.0.0"
    else:
        if is_routable:
            ipv = "public"
        else:
            ipv = "private"
    print(ip,ipv)
    '''


    
    #using humanize 
    rand = timezone.now()

    if perm == 0 :
        error = "Access Denied"
        return render(request, 'back/error.html', {'error':error})
    #user permission check end

    #using random library to get random number
    #rand = random.randint(0, 100000000)

    return render(request, 'back/home.html', {'rand':rand})

#function to login
def mylogin(request):
    if request.method == 'POST':
        utxt = request.POST.get('username')
        ptxt = request.POST.get('password')


        if utxt != "" and ptxt != "":
            user = authenticate(username=utxt, password=ptxt)
            if user != None:
                login(request, user)
                return redirect('home')       


    return render(request, 'back/login.html')

#function to register/signup
def myregister(request):

    #get data from the register form template
    if request.method == "POST":
        fname = request.POST.get('fname')
        lname = request.POST.get('lname')
        uname = request.POST.get('uname')
        email = request.POST.get('email')
        password1 = request.POST.get('password1')
        password2 = request.POST.get('password2')

        #check for empty name
        if fname == "" and lname == "" :
            msg = "Please enter your name"
            return render(request, 'back/msgbox.html', {'msg':msg})

        #check password validity
        if password1 != password2:
            msg = "Your Password Didn't Matched!"
            return render(request, 'back/msgbox.html', {'msg':msg})

        #check if password contains numbers, digit and symbols
        count1 = 0
        count2 = 0
        count3 = 0
        count4 = 0
        for i in password1 :
            if i > "0" and i < "9" :
                count1 = 1
            if i > "A" and i < "Z" :
                count2 = 1
            if i > "a" and i < "z" :
                count3 = 1
            if i > "!" and i < "_" :
                count4 = 1
        
        if count1 == 0 and count2 == 0 and count3 == 0 and count4 == 0 :
            msg = "Your password is weak"
            return render(request, 'back/msgbox.html', {'msg':msg})

        if len(password1) < 8:
            error = "Your password must be atleast 8 characters"
            return render(request, 'back/msgbox.html', {'error':error})
        
        #check username and email not exist
        if len(User.objects.filter(username=uname)) == 0 and len(User.objects.filter(email=email)) == 0 :

            #get user ip
            ip, is_routable = get_client_ip(request)
            if ip is None:
                ip = "0.0.0.0"
            
            #get user location
            try:
                response = DbIpCity.get(ip,api_key='free')
                country = response.country + " | " + response.city
            except:
                country = "Unknown"
            
                user = User.objects.create_user(username=uname, email=email, password=password1)
                db = ManageUser(fname=fname, lname=lname, txt=uname, email=email, ip=ip, country=country)
                db.save()

            
    return render(request, 'back/login.html')

#logout function
def mylogout(request):
    logout(request)

    return redirect('mylogin')

#function for change password
def change_pass(request):

    #check user login start
    if request.user.is_authenticated:
        redirect('mylogin')
    #check user login end

    if request.method == "POST":
        oldpass = request.POST.get('oldpass')
        newpass = request.POST.get('newpass')

        if oldpass == "" or newpass == "" :
            error = "All fields required!"
            return render(request, 'back/error.html', {'error':error})
    
        user = authenticate(username=request.user, password=oldpass)
        if user != None:
            if len(newpass) < 8:
                error = "Your Password must be at least 8 characters"
                return render(request, 'back/error.html', {'error': error})
            
            count1 = 0
            count2 = 0
            count3 = 0
            count4 = 0
            for i in newpass:
                if i > "0" and i < "9" :
                    count1 = 1
                if i > "A" and i < "Z" :
                    count2 = 1
                if i > "a" and i < "z" :
                    count3 = 1
                if i > "!" and i < "_" :
                    count4 = 1
            
            if count1 == 1 and count2 == 1 and count3 == 1 and count4 == 1 :
                #print(count1, count2,count3, count4)
                user = User.objects.get(username=request.user)
                user.set_password(newpass)
                user.save()
                return redirect('mylogout')
         

        else:
            error = "Incorrect password. '\n At least, 1 uppercase, 1 lowercase, 1 digit and 1 symbol required"
            return render(request, 'back/error.html', {'error':error})

    return render(request, 'back/changepass.html')

#function for site setting
def site_setting(request):
    #user login check start
    if not request.user.is_authenticated:
        return redirect('mylogin')
    #user login check end

    #user access check start
    perm = 0
    for i in request.user.groups.all():
        if i.name == "masteruser": perm = 1
    if perm == 0:
        error = "Access Denied!"
        return render(request, 'back/error.html', {'error':error})
    #user access check end

    if request.method == 'POST':
        #get required field from the form
        name = request.POST.get('name')
        tell = request.POST.get('tell')
        fb = request.POST.get('fb')
        tw = request.POST.get('tw')
        yt = request.POST.get('yt')
        pint = request.POST.get('pint')
        gplus = request.POST.get('gplus')
        link = request.POST.get('link')
        text = request.POST.get('text')
        seo_text = request.POST.get('seotxt')
        seo_keyword = request.POST.get('seokeyword')


        #check for social media empty string
        if fb == "": fb = "#"
        if tw == "": tw = "#"
        if yt == "": yt = "#"
        if pint == "": pint = "#"
        if gplus == "": gplus = "#"
        if link == "": link = "#"

        #to check for empty field
        if name == "" or tell == "" or text == "":
            error = "All field requierd!"
            return render(request, 'back/error.html', {'error':error})

        try: #try/except for the 1st image
            #save image file
            myfile = request.FILES['myfile']
            fs = FileSystemStorage()
            filename = fs.save(myfile.name, myfile)
            url = fs.url(filename)

            picurl = url
            picname = filename

        except:
            #set file to 'no image'
            picurl = "no image"
            picname = "no image"
        
        #setting try/except for the second image
        try:
            #save image file
            myfile2 = request.FILES['myfile2']
            fs2 = FileSystemStorage()
            filename2 = fs2.save(myfile2.name, myfile2)
            url2 = fs2.url(filename2)

            picurl2 = url2
            picname2 = filename2

        except:
            #set file to 'no image'
            picurl2 = "no image"
            picname2 = "no image"

        #update required field
        db = Main.objects.get(pk=1)
        db.name = name
        db.tell = tell
        db.fb = fb
        db.tw = tw
        db.yt = yt
        db.pint = pint
        db.gplus = gplus
        db.link = link
        db.about = text
        db.seo_text = seo_text
        db.seo_keyword = seo_keyword

        #dont change the field if picurl field is not empty
        if picurl != "no image": db.picurl = picurl 
        if picname != "no image": db.picname = picname
        if picurl2 != "no image": db.picurl2 = picurl2
        if picname2 != "no image": db.picname2 = picname2    
        
        db.save()
   
    #get new data and save it
    site = Main.objects.get(pk=1)



    return render(request, 'back/setting.html', {'site':site})

#create about_setting() function
def about_setting(request):
    #login check start
    if not request.user.is_authenticated:
        return redirect('mylogin')
    #login check end

    #user access check start
    perm = 0
    for i in request.user.groups.all():
        if i.name == "masteruser": perm = 1
    if perm == 0:
        error = "Access Denied!"
        return render(request, 'back/error.html', {'error':error})
    #user access check end

    if request.method == 'POST':
        text = request.POST.get('text')

        if text == "":
            error = "Please enter your brief discription"
            return render(request, 'back/error.html', {'error':error})     
       
        #update required field
        db = Main.objects.get(pk=1)
        
        db.about = text                
        db.save()
        
    about = Main.objects.get(pk=1)


    return render(request, 'back/about_setting.html', {'about':about})


#rest_framework
class NewsViewSet(viewsets.ModelViewSet):
    queryset = News.objects.all()
    serializer_class = NewsSerializer
    

def show_data(request):

    count = Newsletter.objects.filter(status=1).count()

    data = {'Count':count}

    return JsonResponse(data)

   
