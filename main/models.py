from __future__ import unicode_literals
from django.db import models


# Create your models here.

class Main(models.Model):
    name = models.CharField(max_length=30, default="tboost")
    about = models.TextField()
    aboutext = models.TextField(default="None")
    fb = models.CharField(max_length=30)
    tw = models.CharField(max_length=30)
    yt = models.CharField(max_length=30)
    pint = models.CharField(max_length=30, default='None')
    gplus = models.CharField(max_length=30, default='None')
    tell = models.CharField(default='-', max_length=30)
    link = models.CharField(default='site_link', max_length=30)
    phone = models.IntegerField(default=0)
    city = models.CharField(max_length=100, default='-')

    seo_text = models.CharField(max_length=200, default='-')
    seo_keyword = models.TextField(default='-')


    set_name = models.CharField(default='site settings', max_length=30)

    picurl = models.CharField(default='image', max_length=30)
    picname = models.CharField(default='filename', max_length=30)

    picurl2 = models.CharField(default='image', max_length=30)
    picname2 = models.CharField(default='filename', max_length=30)

    picurl3 = models.CharField(default='image', max_length=30)
    picname3 = models.CharField(default='filename', max_length=30)



    def __str__(self):
        return self.set_name + ' | ' + ' pk = ' + str(self.pk)
