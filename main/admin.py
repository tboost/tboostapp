from django.contrib import admin
from .models import Main
#set permission
from django.contrib.auth.models import Permission

# Register your models here.
admin.site.register(Main)

#register your permissions
admin.site.register(Permission)
