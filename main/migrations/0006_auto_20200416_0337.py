# Generated by Django 3.0.2 on 2020-04-16 02:37

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0005_auto_20200409_1020'),
    ]

    operations = [
        migrations.AddField(
            model_name='main',
            name='seo_keyword',
            field=models.TextField(default='-'),
        ),
        migrations.AddField(
            model_name='main',
            name='seo_text',
            field=models.CharField(default='-', max_length=200),
        ),
    ]
