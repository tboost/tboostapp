from django.shortcuts import render, get_object_or_404, redirect
from .models import Cat
from django.contrib.auth.models import User, Group, Permission
import csv
from django.http import HttpResponse



#create your views here...


#function to create new category
def cat_add(request):
    #user check start
    if not request.user.is_authenticated:
        return redirect('mylogin')
    #user check end

    #user access check start
    perm = 0
    for i in request.user.groups.all():
        if i.name == "masteruser": perm = 1
    if perm == 0:
        error = "Access Denied!"
        return render(request, 'back/error.html', {'error':error})
    #user access check end

    if request.method == 'POST':
        name = request.POST.get('name')

        if name == "":
            error = 'empty field not allowed'
            return render(request, 'back/error.html', {'error':error})

            #check for duplicate category
        if len(Cat.objects.filter(name=name)) != 0:

            error = "category already exist! "
            return render(request, 'back/error.html', {'error':error})

        #save category name into database
        db = Cat(name=name)
        db.save()
        return redirect(cat_list)

    return render(request, 'back/cat_add.html')


#create category list function
def cat_list(request):
    #user check start
    if not request.user.is_authenticated:
        return redirect('mylogin')
    #user check end

    #user access check start
    perm = 0
    for i in request.user.groups.all():
        if i.name == "masteruser": perm = 1
    if perm == 0:
        error = "Access Denied!"
        return render(request, 'back/error.html', {'error':error})
    #user access check end

    cat = Cat.objects.all()

    return render(request, 'back/cat_list.html', {'cat':cat})


#create category list function
def cat_del(request, pk):
    #user check start
    if not request.user.is_authenticated:
        return redirect('mylogin')
    #user check end

    #user access check start
    perm = 0
    for i in request.user.groups.all():
        if i.name == "masteruser": perm = 1
    if perm == 0:
        error = "Access Denied!"
        return render(request, 'back/error.html', {'error':error})
    #user access check end

    cat = Cat.objects.filter(pk=pk)
    cat.delete()

    return redirect('cat_list')



#function to publish news categories
def cat_publish(request, pk):
    #user check start
    if not request.user.is_authenticated:
        return redirect('mylogin')
    #user check end

    #user permission check start
    perm = 0
    perms = Permission.objects.filter(user=request.user)
    for i in perms:
        if i.codename == "master_user" : perm = 1
    if perm == 0 :
        error = "Access Denied"
        return render(request, 'back/error.html', {'error':error})
    #user permission check end

    cat = Cat.objects.get(pk=pk)
    cat.act = 1
    cat.save()
  

    return redirect('cat_list')


#function to download csv file
def export_cat_csv(request):

    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="cat.csv"'
    
    writer = csv.writer(response)
    writer.writerow(['Title', 'Counter'])

    #get all category objects from database
    for i in Cat.objects.all():
        writer.writerow([i.name, i.count])

    return response

#function to import data
def import_cat_csv(request):

    if request.method == "POST" :

        #receive the file
        csv_file = request.FILES['csv_file']

        #check for file extension 
        if not csv_file.name.endswith('.csv'):
            error = "Invalid format! CSV files only"
            return render(request, 'back/error.html', {'error':error})
        
        #check for file size
        if csv_file.multiple_chunks() :
            error = "file too large!"
            return render(request, 'back/error.html', {'error':error})

        #read/convert data as utf-8
        file_data = csv_file.read().decode("utf-8")
        lines = file_data.split("\n")

        for line in lines :
            fields = line.split(",") #separate by comma
            try:
                #check for duplicate data and field data not same as headings/Title
                if len(Cat.objects.filter(name=fields[0])) == 0 and fields[0] != "Title" and fields[0] != "" :
                    b = Cat(name=fields[0])
                    b.save()

            except:
                print('Finish')


    return redirect('cat_list')

    

