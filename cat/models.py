from __future__ import unicode_literals
from django.db import models
from django.utils import timezone

# Create your models here.

class Cat(models.Model):
    name = models.CharField(max_length=50, unique=True)
    count = models.IntegerField(default=0)
    act = models.IntegerField(default=0)
    
    
    #set_name = models.CharField(default='news settings', max_length=30)

    
    def __str__(self):
        return self.name

