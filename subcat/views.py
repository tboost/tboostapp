from django.shortcuts import render, get_object_or_404, redirect
from .models import Subcat
from cat.models import Cat
from django.contrib.auth.models import User, Group, Permission



#create your views here...

#function to view sub_category lists
def subcat_list(request):
    #user check start
    if not request.user.is_authenticated:
        return redirect('mylogin')
    #user check end

    #user access check start
    perm = 0
    for i in request.user.groups.all():
        if i.name == "masteruser": perm = 1
    if perm == 0:
        error = "Access Denied!"
        return render(request, 'back/error.html', {'error':error})
    #user access check end

    subcat = Subcat.objects.all()


    return render(request, 'back/subcat_list.html', {'subcat':subcat})


#function to create new category
def subcat_add(request):
    #user check start
    if not request.user.is_authenticated:
        return redirect('mylogin')
    #user check end

#user access check start
    perm = 0
    for i in request.user.groups.all():
        if i.name == "masteruser": perm = 1
    if perm == 0:
        error = "Access Denied!"
        return render(request, 'back/error.html', {'error':error})
    #user access check end

    #get all categories
    cat = Cat.objects.all()

    if request.method == 'POST':
        name = request.POST.get('name')
        catid = request.POST.get('cat')

        if name == "":
            error = 'empty field not allowed'
            return render(request, 'back/error.html', {'error':error})

            #check for duplicate category
        if len(Subcat.objects.filter(name=name)) != 0:

            error = "subcategory already exist!"
            return render(request, 'back/error.html', {'error':error})
        
        #get category name field and send it to catname
        catname = Cat.objects.get(pk=catid).name

        db = Subcat(name=name, catname=catname, catid=catid)
        db.save()
        return redirect('subcat_list')
        
    return render(request, 'back/subcat_add.html', {'cat':cat})


def subcat_del(request, pk):
    #user check start
    if not request.user.is_authenticated:
        return redirect('mylogin')
    #user check end

    #user access check start
    perm = 0
    for i in request.user.groups.all():
        if i.name == "masteruser": perm = 1
    if perm == 0:
        error = "Access Denied!"
        return render(request, 'back/error.html', {'error':error})
    #user access check end

    subcat = Subcat.objects.filter(pk=pk)
    subcat.delete()

    return redirect('subcat_list')

#function to publish news categories
def subcat_publish(request, pk):
    #user check start
    if not request.user.is_authenticated:
        return redirect('mylogin')
    #user check end

    #user permission check start
    perm = 0
    perms = Permission.objects.filter(user=request.user)
    for i in perms:
        if i.codename == "master_user" : perm = 1
    if perm == 0 :
        error = "Access Denied"
        return render(request, 'back/error.html', {'error':error})
    #user permission check end

    subcat = Subcat.objects.get(pk=pk)
    subcat.act = 1
    subcat.save()
  

    return redirect('subcat_list')
