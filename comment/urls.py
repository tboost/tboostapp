from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^comment/news/add/(?P<pk>\d+)/$', views.news_comment_add, name="news_comment_add"),
    url(r'^panel/comments/list/$', views.comments_list, name="comments_list"),
    url(r'^panel/comments/del/(?P<pk>\d+)/$', views.comments_del, name="comments_del"),
    url(r'^panel/comments/confirmed/(?P<pk>\d+)/$', views.comments_confirmed, name="comments_confirmed"),
    
    
]