from django.shortcuts import render, get_object_or_404, redirect
from .models import Comment
from news.models import News
from main.models import Main
from manageuser.models import ManageUser
from django.core.files.storage import FileSystemStorage
from django.utils import timezone
from subcat.models import Subcat
from cat.models import Cat
from trending.models import Trending
from contactform.models import ContactForm
import random
from random import randint
from django.contrib.auth.models import User, Group, Permission
import datetime


#Your views function here...

#function to add comment
def news_comment_add(request, pk): #Note: the 'pk' is the pk of the news

    if request.method == "POST" :
        comment = request.POST.get('msg')

        #check user login
        if request.user.is_authenticated :
            manager = ManageUser.objects.get(txt=request.user)
            db = Comment(name=manager.name, email=manager.email, comment=comment, news_id=pk, date=timezone.now())
            db.save()
        else:
        #get data of non authenticated user
            name = request.POST.get('name')
            email = request.POST.get('email')
            comment = request.POST.get('msg')
            b = Comment(name=name, email=email, comment=comment, news_id=pk, date=timezone.now())
            b.save()

    newsname = News.objects.get(pk=pk).name
    
    return redirect('news_detail', word=newsname)

#function show comments list
def comments_list(request):

    #user check start
    if not request.user.is_authenticated:
        return redirect('mylogin')
    #user check end

    #user access check start
    perm = 0
    for i in request.user.groups.all():
        if i.name == "masteruser": perm = 1
    if perm == 0:
        a = News.objects.get(pk=pk).writer
        if str(a) != str(request.user):  
            error = "Access Denied!"
            return render(request, 'back/error.html', {'error':error})
    #user access check end

    comment = Comment.objects.all()


    return render(request, 'back/comments_list.html', {'comment':comment})

#function to delete comment
def comments_del(request, pk):

    #user check start
    if not request.user.is_authenticated:
        return redirect('mylogin')
    #user check end

    #user access check start
    perm = 0
    for i in request.user.groups.all():
        if i.name == "masteruser": perm = 1
    if perm == 0:
        a = News.objects.get(pk=pk).writer
        if str(a) != str(request.user):  
            error = "Access Denied!"
            return render(request, 'back/error.html', {'error':error})
    #user access check end

    comment = Comment.objects.filter(pk=pk)
    comment.delete()


    return redirect('comments_list')


#function to delete comment
def comments_confirmed(request, pk):

    #user check start
    if not request.user.is_authenticated:
        return redirect('mylogin')
    #user check end

    #user access check start
    perm = 0
    for i in request.user.groups.all():
        if i.name == "masteruser": perm = 1
    if perm == 0:
        a = News.objects.get(pk=pk).writer
        if str(a) != str(request.user):  
            error = "Access Denied!"
            return render(request, 'back/error.html', {'error':error})
    #user access check end

    comment = Comment.objects.get(pk=pk)
    comment.status = 1
    comment.save()


    return redirect('comments_list')
