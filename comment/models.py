from __future__ import unicode_literals
from django.db import models
from django.utils import timezone

# Create your models here.

class Comment(models.Model):
    name = models.CharField(max_length=50)
    comment = models.TextField()
    email = models.EmailField()
    news_id = models.IntegerField()
    status = models.IntegerField(default=0)
    date = models.DateTimeField()

    
    def __str__(self):
        return self.name

