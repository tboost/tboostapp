# Generated by Django 3.0.2 on 2020-01-29 02:14

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0013_auto_20200122_1440'),
    ]

    operations = [
        migrations.AddField(
            model_name='news',
            name='tag',
            field=models.TextField(default='none'),
        ),
    ]
