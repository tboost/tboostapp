from __future__ import unicode_literals
from django.db import models
from django.utils import timezone

# Create your models here.

class News(models.Model):
    name = models.CharField(max_length=100)
    short_txt = models.TextField()
    body_txt = models.TextField()
    date = models.DateTimeField()
    picname = models.CharField(max_length=50)
    picurl = models.CharField(default="none", max_length=50)
    writer = models.CharField(max_length=50)
    catname = models.CharField(max_length=30, default='none')
    catid = models.IntegerField(default=0)
    ocatid = models.IntegerField(default=0)
    email = models.EmailField(default='@tboost.com')
    show = models.IntegerField(default=0)
    tag = models.TextField(default='none')
    act = models.IntegerField(default=0)
    rand = models.CharField(default=0, max_length=50)  


    #set_name = models.CharField(default='news settings', max_length=30)

    
    def __str__(self):
        return self.name

