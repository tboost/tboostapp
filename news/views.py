from django.shortcuts import render, get_object_or_404, redirect
from .models import News
from main.models import Main
from django.core.files.storage import FileSystemStorage
from django.utils import timezone
from subcat.models import Subcat
from cat.models import Cat
from manageuser.models import ManageUser
from trending.models import Trending
from contactform.models import ContactForm
from comment.models import Comment
import random
from random import randint
from django.contrib.auth.models import User, Group, Permission
import datetime
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from itertools import chain #use to merge 2 queries 


#define a global variable for search
mysearch = ''

#function to view news detail
def news_detail(request,word):

    site = Main.objects.get(pk=1)
    news = News.objects.filter(act=1).order_by('-date')
    cat = Cat.objects.all()
    subcat = Subcat.objects.all()
    lastnews = News.objects.filter(act=1).order_by('-pk')[:3] #at the top of the page (1st container)

    shownews = News.objects.filter(name=word)
    popnews = News.objects.filter(act=1).order_by('-show')
    popnews1 = News.objects.filter(act=1).order_by('-show')[:4]
    trending = Trending.objects.all().order_by('-pk')[:5]

    #trending random display
    random_object = Trending.objects.all()[randint(0, len(trending) -1)]
    
    tagname = News.objects.get(name=word).tag 
    tag = tagname.split(',')

     #show no. news viewed
    try: #show how many times a news is viewed
        mynews = News.objects.get(name=word)
        mynews.show = mynews.show + 1
        mynews.save()
    except:
        print("Can't Add Show")
    
    code = News.objects.get(name=word).pk

    comment = Comment.objects.filter(news_id=code, status=1).order_by('-pk')[:5]
    comment_count = len(comment)

    link = "/urls/" + str(News.objects.get(name=word).rand)
    


    return render(request, 'front/news_detail.html', {'site':site, 'news':news, 'cat':cat, 'subcat':subcat, 'lastnews':lastnews, 'shownews':shownews, 'popnews':popnews, 'popnews1':popnews1, 'tag':tag, 'trending':trending, 'random_object':random_object, 'code':code, 'comment':comment, 'comment_count':comment_count, 'link':link, 'tagname':tagname})


#create a function to view news detail
def news_detail_short(request,word):

    site = Main.objects.get(pk=1)
    news = News.objects.filter(act=1).order_by('-date')
    cat = Cat.objects.all()
    subcat = Subcat.objects.all()
    lastnews = News.objects.filter(act=1).order_by('-pk')[:3] #at the top of the page (1st container)

    shownews = News.objects.filter(rand=word)
    popnews = News.objects.filter(act=1).order_by('-show')
    popnews1 = News.objects.filter(act=1).order_by('-show')[:4]
    trending = Trending.objects.all().order_by('-pk')[:5]

    #trending random display
    random_object = Trending.objects.all()[randint(0, len(trending) -1)]
    
    tagname = News.objects.get(rand=word).tag 
    tag = tagname.split(',')

     #show no. news viewed
    try:
        mynews = News.objects.get(rand=word)
        mynews.show = mynews.show + 1
        mynews.save()
    except:
        print("Can't Add Show")
    
    code = News.objects.get(rand=word).pk

    short_link = "/urls/" + str(News.objects.get(rand=word).rand)


    return render(request, 'front/news_detail.html', {'site':site, 'news':news, 'cat':cat, 'subcat':subcat, 'lastnews':lastnews, 'shownews':shownews, 'popnews':popnews, 'popnews1':popnews1, 'tag':tag, 'trending':trending, 'random_object':random_object, 'code':code, 'short_link':short_link})


#function to create a new news
def add_news(request):
    #user check start
    if not request.user.is_authenticated:
        return redirect('mylogin')
    #user check end

    #user permission check start
    perm = 0
    perms = Permission.objects.filter(user=request.user)
    for i in perms:
        if i.codename == "master_user" : perm = 1
    if perm == 0 :
        error = "Access Denied"
        return render(request, 'back/error.html', {'error':error})
    #user permission check end

    #generate random number for news post
    randint = str(random.randint(1000,9999))
    date = str(datetime.datetime.now())
    rand = date + randint
    rand = str(rand)

    while len(News.objects.filter(rand=rand)) != 0 :
        randint = str(random.randint(1000,9999))
        rand = date + randint
        rand = str(rand)

    #get all subcategories
    cat = Subcat.objects.all()

    if request.method == 'POST':
        newstitle = request.POST.get('newstitle')
        newscat = request.POST.get('newscat')  
        newsid = request.POST.get('newscat') 
        mymail = request.POST.get('mymail')     
        shorttxt = request.POST.get('shorttxt')
        text = request.POST.get('textarea-ckeditor')
        tag = request.POST.get('tag')
        
        #check for empty fields
        if newstitle == "" or newscat == "" or shorttxt == "" or text == "" or mymail == "":
            error = "**All fields required**"
            return render(request, 'back/error.html', {'error':error})
        
        #set file upload
        try:
            myfile = request.FILES['myfile']
            fs = FileSystemStorage()
            filename = fs.save(myfile.name, myfile)
            url = fs.url(filename)

            #check if file is an image
            if str(myfile.content_type).startswith('image'):

                #check if image size is greater than 5MB
                if myfile.size < 5000000:

                    #get the category name using the newsid
                    newsname = Subcat.objects.get(pk=newsid).name
                    #get the original category id
                    ocatid = Subcat.objects.get(pk=newsid).catid

                    #save to database
                    
                    db = News(name=newstitle, short_txt=shorttxt, body_txt=text, date=timezone.now(), picname=filename, picurl=url, writer=request.user.get_full_name(), catname=newsname, catid=newsid, email=mymail, show=0, ocatid=ocatid, tag=tag, rand=rand)
                    
                    db.save()

                    #get the number of the news from ocatid
                    count = len(News.objects.filter(ocatid=ocatid))

                    #update the count field in Cat models
                    db = Cat.objects.get(pk=ocatid)
                    db.count = count
                    db.save()
                    #redirect to news_list page
                    return redirect(news_list)

                else:
                    #remove the file from the media folder
                    fs = FileSystemStorage()
                    fs.delete(news.filename)

                    #return error message
                    error = "File size must not be greater than 5MB"
                    return render(request, 'back/error.html', {'error':error})
                
            else:
                #delete the file from the media folder
                fs = FileSystemStorage()
                fs.delete(news.filename)

                #return error messages
                error = "File Not Supported. Image file only"
                return render(request, 'back/error.html', {'error':error})
              
        except:
            error = "Please upload your image"
            return render(request, 'back/error.html', {'error':error})

    return render(request, 'back/add_news.html', {'cat':cat})
    

#function for news list
def news_list(request):
    #user check start
    if not request.user.is_authenticated:
        return redirect('mylogin')
    #user check end

    #user access check start
    perm = 0
    for i in request.user.groups.all():
        if i.name == "masteruser": perm = 1
    if perm == 0:
        news = News.objects.filter(writer=request.user)
    elif perm == 1 :
        newss = News.objects.all()
        paginator = Paginator(newss,5) #returns 2 on each page
        page = request.GET.get('page')

        try:
            news = paginator.page(page)
        except EmptyPage: #if page is empty
            news = paginator.page(paginator.num_pages)
        except PageNotAnInteger:
            news = paginator.page(1)


    #user access check end
    
    

    return render(request, 'back/news_list.html', {'news':news})


#function to delete records or files
def del_news(request, pk):
    #user check start
    if not request.user.is_authenticated:
        return redirect('mylogin')
    #user check end

    #user permission check start
    perm = 0
    perms = Permission.objects.filter(user=request.user)
    for i in perms:
        if i.codename == "master_user" : perm = 1
    if perm == 0 :
        error = "Access Denied"
        return render(request, 'back/error.html', {'error':error})
    #user permission check end

    #user access check start
    perm = 0
    for i in request.user.groups.all():
        if i.name == "masteruser": perm = 1
    if perm == 0:
        a = News.objects.get(pk=pk).writer
        if str(a) != str(request.user): 
            error = "Access Denied!"
            return render(request, 'back/error.html', {'error':error})
    #user access check end

    try:
        news = News.objects.get(pk=pk)

        fs = FileSystemStorage()
        fs.delete(news.picname)

        #get the original category id of the News to be deleted
        ocatid = News.objects.get(pk=pk).ocatid

        #delete the news
        news.delete()

        #count and to know the category it belongs to e.g politics, sport
        count = len(News.objects.filter(ocatid=ocatid))

        #show categories in Cat where pk=ocatid
        n = Cat.objects.get(pk=ocatid)
        n.count = count
        n.save()

        
    except:
        error = "Something wrong!"
        return render(request, 'back/error.html', {'error':error})

    return redirect('news_list')


#function to edit news
def news_edit(request, pk):
    #user check start
    if not request.user.is_authenticated:
        return redirect('mylogin')
    #user check end

    #user permission check start
    perm = 0
    perms = Permission.objects.filter(user=request.user)
    for i in perms:
        if i.codename == "master_user" : perm = 1
    if perm == 0 :
        error = "Access Denied"
        return render(request, 'back/error.html', {'error':error})
    #user permission check end

    #check for not found news
    if len(News.objects.filter(pk=pk)) == 0:
        error = "News not found"
        return render(request, 'back/error.html', {'error':error})

    #user access check start
    perm = 0
    for i in request.user.groups.all():
        if i.name == "masteruser": perm = 1
    if perm == 0:
        a = News.objects.get(pk=pk).writer
        if str(a) != str(request.user):  
            error = "Access Denied!"
            return render(request, 'back/error.html', {'error':error})
    #user access check end

    #display sticky form
    #news = News.objects.filter(pk=pk)  #Note: using filter method, you will need to run for loop in your news edit page to make your news contents sticky.

    news = News.objects.get(pk=pk)  #Using get(), you call the object direct in your news edit page.
    cat = Subcat.objects.all()

    if request.method == 'POST':
        newstitle = request.POST.get('newstitle')
        newscat = request.POST.get('newscat')  
        newsid = request.POST.get('newscat') 
        mymail = request.POST.get('mymail')     
        shorttxt = request.POST.get('shorttxt')
        text = request.POST.get('textarea-ckeditor')
        tag = request.POST.get('tag')
        
        #check for empty fields
        if newstitle == "" or newscat == "" or shorttxt == "" or text == "" or mymail == "":
            error = "**All fields required**"
            return render(request, 'back/error.html', {'error':error})
        
        #set file upload
        try:
            myfile = request.FILES['myfile']
            fs = FileSystemStorage()
            filename = fs.save(myfile.name, myfile)
            url = fs.url(filename)

            #check if file is an image
            if str(myfile.content_type).startswith('image'):

                #check if image size is greater than 5MB
                if myfile.size < 5000000:

                    #get the category name using the newsid
                    newsname = Subcat.objects.get(pk=newsid).name

                    #save to database
                    db = News.objects.get(pk=pk)

                    # to delete the previous image and set new ones
                    fs_new = FileSystemStorage()
                    fs_new.delete(news.picname)

                    db.name = newstitle
                    db.email = mymail
                    db.short_txt = shorttxt
                    db.body_txt = text
                    db.picname = filename
                    db.picurl = url
                    db.writer = 'none'
                    db.catname = newsname
                    db.catid = newsid
                    db.tag = tag
                    db.act = 0


                    db.save()
                    #redirect to news_list page
                    return redirect(news_list)

                else:
                    #remove the file from the media folder
                    fs = FileSystemStorage()
                    fs.delete(news.filename)

                    #return error message
                    error = "File size must not be greater than 5MB"
                    return render(request, 'back/error.html', {'erro':error})
                
            else:
                #delete the file from the media folder
                fs = FileSystemStorage()
                fs.delete(news.filename)

                #return error messages
                error = "File Not Supported. Image file only"
                return render(request, 'back/error.html', {'error':error})
              
        except:
            #if the user did not change the image, run below codes

            #get the category name using the newsid
            newsname = Subcat.objects.get(pk=newsid).name

            #save to database
            db = News.objects.get(pk=pk)

            db.name = newstitle
            db.email = mymail
            db.short_txt = shorttxt
            db.body_txt = text
            db.catname = newsname
            db.catid = newsid
            db.tag = tag

            db.save()
            #redirect to news_list page
            return redirect(news_list)

    return render(request, 'back/news_edit.html', {'pk':pk, 'news':news, 'cat':cat})



#function to publish news
def news_publish(request, pk):
    #user check start
    if not request.user.is_authenticated:
        return redirect('mylogin')
    #user check end

    #user permission check start
    perm = 0
    perms = Permission.objects.filter(user=request.user)
    for i in perms:
        if i.codename == "master_user" : perm = 1
    if perm == 0 :
        error = "Access Denied"
        return render(request, 'back/error.html', {'error':error})
    #user permission check end

    news = News.objects.get(pk=pk)
    news.act = 1
    news.save()
  

    return redirect('news_list')


#function to show all news
def news_all_show(request, word):

    #get the pk of the catid
    catid = Cat.objects.get(name=word).pk
    #filter news by catid to get all news by category
    allnewss = News.objects.filter(ocatid=catid)

    site = Main.objects.get(pk=1)
    news = News.objects.filter(act=1).order_by('-date')
    cat = Cat.objects.filter(act=1)
    subcat = Subcat.objects.filter(act=1)
    lastnews = News.objects.filter(act=1).order_by('-pk')[:3]
    popnews = News.objects.filter(act=1).order_by('-show')[:5]
    popnews1 = News.objects.filter(act=1).order_by('-show')[:4]
    trending = Trending.objects.all().order_by('-pk')[:5]
    lastnews2 = News.objects.filter(act=1).order_by('-pk')[:4]
    #trending random display
    random_object = Trending.objects.all()[randint(0, len(trending) -1)]

    #randomly choose lastnews from the database
    rand_lastnews = News.objects.all()[randint(0, len(lastnews) -1)]

    
    paginator = Paginator(allnewss,3) #returns 2 on each page
    page = request.GET.get('page')

    try:
        allnews = paginator.page(page)
    except EmptyPage: #if page is empty
        allnews = paginator.page(paginator.num_pages)
    except PageNotAnInteger:
        allnews = paginator.page(1)

        
    return render(request, 'front/all_news.html', {'allnews':allnews, 'site':site, 'news':news, 'cat':cat, 'subcat':subcat, 'lastnews':lastnews, 'popnews':popnews, 'popnews1':popnews1, 'trending':trending, 'random_object':random_object, 'rand_lastnews':rand_lastnews, 'lastnews2':lastnews2})



#all news function
#function to show all news
def all_news(request):

    #display all news objects
    allnewss = News.objects.all()

    site = Main.objects.get(pk=1)
    news = News.objects.filter(act=1).order_by('-date')
    cat = Cat.objects.filter(act=1)
    subcat = Subcat.objects.filter(act=1)
    lastnews = News.objects.filter(act=1).order_by('-pk')[:3]
    popnews = News.objects.filter(act=1).order_by('-show')[:5]
    popnews1 = News.objects.filter(act=1).order_by('-show')[:4]
    trending = Trending.objects.all().order_by('-pk')[:5]
    lastnews2 = News.objects.filter(act=1).order_by('-pk')[:4]
    #trending random display
    random_object = Trending.objects.all()[randint(0, len(trending) -1)]

    #randomly choose lastnews from the database
    rand_lastnews = News.objects.all()[randint(0, len(lastnews) -1)]

    
    #set from and to date of the news category (i.e search within a range of time)
    f_rom = []
    t_o = []

    for i in range(30):
        b = datetime.datetime.now() - datetime.timedelta(days=i)
        year = b.year
        month = b.month
        day = b.day

        if len(str(day)) == 1:
            day = "0" + str(day)
        if len(str(month)) == 1:
            month = "0" + str(month)

        b = str(year) + "/" + str(month) + "/" + str(day)
        f_rom.append(b)
        print(b)
        
    print(f_rom)


    for i in range(30):
        b = datetime.datetime.now() - datetime.timedelta(days=i)
        year = b.year
        month = b.month
        day = b.day
        b = str(year) + "/" + str(month) + "/" + str(day)
        t_o.append(b)
        print(b)
        
    print(t_o)

    
    paginator = Paginator(allnewss,10) #returns 2 on each page
    page = request.GET.get('page')

    try:
        allnews = paginator.page(page)
    except EmptyPage: #if page is empty
        allnews = paginator.page(paginator.num_pages)
    except PageNotAnInteger:
        allnews = paginator.page(1)

        
    return render(request, 'front/allnews2.html', {'allnews':allnews, 'site':site, 'news':news, 'cat':cat, 'subcat':subcat, 'lastnews':lastnews, 'popnews':popnews, 'popnews1':popnews1, 'trending':trending, 'random_object':random_object, 'rand_lastnews':rand_lastnews, 'lastnews2':lastnews2, 'f_rom':f_rom, 't_o':t_o})


#function to search all news
def search_all_news(request):

    if request.method == "POST":
        search = request.POST.get('search') #full-text search
        catid = request.POST.get('cat') #search by category

        f_rom = request.POST.get('from') #search by date
        t_o = request.POST.get('to') #search by date

        mysearch = search
        #print(f_rom, t_o)
        # print(search)
        if f_rom != "0" and t_o != "0":
            #check if t_o date is greater than f_rom date
            if t_o < f_rom :
                msg = "Date Inconsistency! Your To date must be lower than From date"
                return render(request, 'back/msgbox.html', {'msg':msg})


        #query set. search by name, short_txt and body_txt
        if catid == '0': #check if category id = 0, then do a full-text search
            #check if date (both from and to date) were selected or not
            if f_rom != "0" and t_o != "0":
                a = News.objects.filter(name__contains=search, date__gte=f_rom, date__lte=t_o)
                b = News.objects.filter(short_txt__contains=search, date__gte=f_rom, date__lte=t_o)
                c = News.objects.filter(body_txt__contains=search, date__gte=f_rom, date__lte=t_o)
            
            #check if only f_rom date are selected
            # elif f_rom != "0":
            #     a = News.objects.filter(name__contains=search, datetime__gte=f_rom)
            #     b = News.objects.filter(short_txt__contains=search, datetime__gte=f_rom)
            #     c = News.objects.filter(body_txt__contains=search, datetime__gte=f_rom)
            
            # #check if only t_o date are selected
            # elif t_o != "0":
            #     a = News.objects.filter(name__contains=search,date__lte=t_o)
            #     b = News.objects.filter(short_txt__contains=search,date__lte=t_o)
            #     c = News.objects.filter(body_txt__contains=search,date__lte=t_o)
            else:
                a = News.objects.filter(name__contains=search)
                b = News.objects.filter(short_txt__contains=search)
                c = News.objects.filter(body_txt__contains=search)
        else:
            #check if date selected or not
            if f_rom != "0" and t_o != "0":
                #check if the category id is not equall to 0, the search by category id
                a = News.objects.filter(name__contains=search, ocatid=catid, date__gte=f_rom, date__lte=t_o)
                b = News.objects.filter(short_txt__contains=search, ocatid=catid, date__gte=f_rom, date__lte=t_o)
                c = News.objects.filter(body_txt__contains=search, ocatid=catid, date__gte=f_rom, date__lte=t_o)
            # elif f_rom != "0":
            #     #check if only f_rom date is selected
            #     a = News.objects.filter(name__contains=search, ocatid=catid, date__gte=f_rom)
            #     b = News.objects.filter(short_txt__contains=search, ocatid=catid, date__gte=f_rom)
            #     c = News.objects.filter(body_txt__contains=search, ocatid=catid, date__gte=f_rom)
            # elif t_o != "0":
            #     #check if only t_o date is selected
            #     a = News.objects.filter(name__contains=search, ocatid=catid, date__lte=t_o)
            #     b = News.objects.filter(short_txt__contains=search, ocatid=catid, date__lte=t_o)
            #     c = News.objects.filter(body_txt__contains=search, ocatid=catid, date__lte=t_o)
            else:
                a = News.objects.filter(name__contains=search, ocatid=catid)
                b = News.objects.filter(short_txt__contains=search, ocatid=catid)
                c = News.objects.filter(body_txt__contains=search, ocatid=catid)

        #merge queries
        allnewss = list(chain(a,b,c))
        #remove duplicate data
        allnewss = list(dict.fromkeys(allnewss))

    else:
        if catid == '0':
            #check if date selected or not
            if f_rom != "0" and t_o != "0" :
                #search the news objects using the global variable (mysearch)
                a = News.objects.filter(name__contains=mysearch, date__gte=f_rom, date__lte=t_o)
                b = News.objects.filter(short_txt__contains=mysearch, date__gte=f_rom, date__lte=t_o)
                c = News.objects.filter(body_txt__contains=mysearch, date__gte=f_rom, date__lte=t_o)
            # elif f_rom != "0" :
            #     #search the news objects using the global variable (mysearch)
            #     a = News.objects.filter(name__contains=mysearch, date__gte=f_rom)
            #     b = News.objects.filter(short_txt__contains=mysearch, date__gte=f_rom)
            #     c = News.objects.filter(body_txt__contains=mysearch, date__gte=f_rom)
            # elif t_o != "0" :
            #     #search the news objects using the global variable (mysearch)
            #     a = News.objects.filter(name__contains=mysearch, date__lte=t_o)
            #     b = News.objects.filter(short_txt__contains=mysearch, date__lte=t_o)
            #     c = News.objects.filter(body_txt__contains=mysearch, date__lte=t_o)
            else:
                a = News.objects.filter(name__contains=mysearch)
                b = News.objects.filter(short_txt__contains=mysearch)
                c = News.objects.filter(body_txt__contains=mysearch)
        else:
            #check if date selected or not
            if f_rom != "0" and t_o != "0":
                #search the news objects using the global variable (mysearch)
                a = News.objects.filter(name__contains=mysearch, ocatid=catid, date__gte=f_rom, date__lte=t_o)
                b = News.objects.filter(short_txt__contains=mysearch, ocatid=catid, date__gte=f_rom, date__lte=t_o)
                c = News.objects.filter(body_txt__contains=mysearch, ocatid=catid, date__gte=f_rom, date__lte=t_o)
            # elif f_rom != "0":
            #     #search the news objects using the global variable (mysearch)
            #     a = News.objects.filter(name__contains=mysearch, ocatid=catid, date__gte=f_rom)
            #     b = News.objects.filter(short_txt__contains=mysearch, ocatid=catid, date__gte=f_rom)
            #     c = News.objects.filter(body_txt__contains=mysearch, ocatid=catid, date__gte=f_rom)
            # elif t_o != "0":
            #     #search the news objects using the global variable (mysearch)
            #     a = News.objects.filter(name__contains=mysearch, ocatid=catid, date__lte=t_o)
            #     b = News.objects.filter(short_txt__contains=mysearch, ocatid=catid, date__lte=t_o)
            #     c = News.objects.filter(body_txt__contains=mysearch, ocatid=catid, date__lte=t_o)
            else:
                a = News.objects.filter(name__contains=mysearch, ocatid=catid)
                b = News.objects.filter(short_txt__contains=mysearch, ocatid=catid)
                c = News.objects.filter(body_txt__contains=mysearch, ocatid=catid)

        #merge queries
        allnewss = list(chain(a,b,c))
        #remove duplicate data
        allnewss = list(dict.fromkeys(allnewss))



    site = Main.objects.get(pk=1)
    news = News.objects.filter(act=1).order_by('-date')
    cat = Cat.objects.filter(act=1)
    subcat = Subcat.objects.filter(act=1)
    lastnews = News.objects.filter(act=1).order_by('-pk')[:3]
    popnews = News.objects.filter(act=1).order_by('-show')[:5]
    popnews1 = News.objects.filter(act=1).order_by('-show')[:4]
    trending = Trending.objects.all().order_by('-pk')[:5]
    lastnews2 = News.objects.filter(act=1).order_by('-pk')[:4]
    #trending random display
    random_object = Trending.objects.all()[randint(0, len(trending) -1)]

    #randomly choose lastnews from the database
    rand_lastnews = News.objects.all()[randint(0, len(lastnews) -1)]

    
    #set from and to date of the news category (i.e search within a range of time)
    f_rom = []
    t_o = []

    for i in range(30):
        b = datetime.datetime.now() - datetime.timedelta(days=i)
        #format date to  display dd/mm/yyyy
        year = b.year
        month = b.month
        day = b.day
        b = str(year) + "/" + str(month) + "/" + str(day)
        f_rom.append(b)
        print(b)
        
    print(f_rom)


    for i in range(30):
        b = datetime.datetime.now() - datetime.timedelta(days=i)

        #format date to display dd/mm/yyyy
        year = b.year
        month = b.month
        day = b.day
        b = str(year) + "/" + str(month) + "/" + str(day)

        t_o.append(b)
        #print(b)
        
    #print(t_o)


    paginator = Paginator(allnewss,10) #returns 2 on each page
    page = request.GET.get('page')

    try:
        allnews = paginator.page(page)
    except EmptyPage: #if page is empty
        allnews = paginator.page(paginator.num_pages)
    except PageNotAnInteger:
        allnews = paginator.page(1)

        
    return render(request, 'front/allnews2.html', {'allnews':allnews, 'site':site, 'news':news, 'cat':cat, 'subcat':subcat, 'lastnews':lastnews, 'popnews':popnews, 'popnews1':popnews1, 'trending':trending, 'random_object':random_object, 'rand_lastnews':rand_lastnews, 'lastnews2':lastnews2, 'f_rom':f_rom, 't_o':t_o})

