from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^article/(?P<word>.*)/$', views.news_detail, name='news_detail'),
    url(r'^panel/news/list/$', views.news_list, name='news_list'),
    url(r'^panel/add/article/$', views.add_news, name='add_news'),
    url(r'^panel/news/edit/(?P<pk>\d+)/$', views.news_edit, name='news_edit'),
    url(r'^panel/del/news/(?P<pk>\d+)/$', views.del_news, name='del_news'),
    url(r'^panel/news/publish/(?P<pk>\d+)/$', views.news_publish, name='news_publish'),
    url(r'^urls/(?P<word>.*)/$', views.news_detail_short, name='news_detail_short'),
    url(r'^front/all/articles/(?P<word>.*)/$', views.news_all_show, name='news_all_show'),
    url(r'^all/posts/$', views.all_news, name='all_news'),
    url(r'^search/all/posts/$', views.search_all_news, name='search_all_news'),

]