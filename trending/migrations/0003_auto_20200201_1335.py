# Generated by Django 3.0.2 on 2020-02-01 12:35

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('trending', '0002_trending_url'),
    ]

    operations = [
        migrations.AlterField(
            model_name='trending',
            name='url',
            field=models.CharField(default='None', max_length=200),
        ),
    ]
