from __future__ import unicode_literals
from django.db import models
from django.utils import timezone

# Create your models here.

class Trending(models.Model):
    text = models.TextField()
    url = models.CharField(default='None', max_length=200)
    date = models.DateTimeField()
    
    


    #set_name = models.CharField(default='news settings', max_length=30)

    
    def __str__(self):
        return self.text

