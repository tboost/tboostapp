from django.shortcuts import render, get_object_or_404, redirect
from .models import Trending
from main.models import Main
from django.core.files.storage import FileSystemStorage
from django.utils import timezone
from subcat.models import Subcat
from cat.models import Cat
from news.models import News
from contactform.models import ContactForm
import random
from random import randint



#function to add trending
def trending_add(request):

    #check user login start
    if not request.user.is_authenticated:
        return redirect('mylogin')
    #check user login end

    #user access check start
    perm = 0
    for i in request.user.groups.all():
        if i.name == "masteruser": perm = 1
    if perm == 0:
        error = "Access Denied!"
        return render(request, 'back/error.html', {'error':error})
    #user access check end

    if request.method == 'POST':
        text = request.POST.get('text')
        url = request.POST.get('url')

        if text == "" or url == "" :
            error = "Empty field not allowed"
            return render(request, 'back/error.html', {'error':error})
    
        db = Trending(text=text, url=url, date=timezone.now())
        db.save()

    return render(request, 'back/trend_add.html')

#trending_list function
def trending_show(request):

    #check user login start
    if not request.user.is_authenticated:
        return redirect('mylogin')
    #check user login end

    #user access check start
    perm = 0
    for i in request.user.groups.all():
        if i.name == "masteruser": perm = 1
    if perm == 0:
        error = "Access Denied!"
        return render(request, 'back/error.html', {'error':error})
    #user access check end

    trending = Trending.objects.all().order_by('-pk')[:5]

    #trending random display
    random_object = Trending.objects.all()[randint(0, len(trending) -1)]


    return render(request, 'back/trend_list.html', {'trending':trending, 'random_object':random_object})


#Trending delete function
def trending_del(request, pk):

    #check user login start
    if not request.user.is_authenticated:
        return redirect('mylogin')
    #check user login end

    #user access check start
    perm = 0
    for i in request.user.groups.all():
        if i.name == "masteruser": perm = 1
    if perm == 0:
        error = "Access Denied!"
        return render(request, 'back/error.html', {'error':error})
    #user access check end

    trend = Trending.objects.filter(pk=pk)
    trend.delete()

    return redirect('trending_show')


#Trending edit function
def trending_edit(request, pk):

    #check user login start
    if not request.user.is_authenticated:
        return redirect('mylogin')
    #check user login end

    #user access check start
    perm = 0
    for i in request.user.groups.all():
        if i.name == "masteruser": perm = 1
    if perm == 0:
        error = "Access Denied!"
        return render(request, 'back/error.html', {'error':error})
    #user access check end

    mytext = Trending.objects.get(pk=pk).text
    myurl = Trending.objects.get(pk=pk).url

    if request.method == "POST":
        text = request.POST.get('text')
        url = request.POST.get('url')

        if text == "" or url == "":
            error = "Empty fields not allowed!"
            return render(request, 'back/error.html', {'error':error})

            db = Trending.objects.get(pk=pk)
            db.text = text
            db.url = url
            db.save()
            return redirect('trending_show')

    return render(request, 'back/trend_edit.html', {'mytext':mytext, 'myurl':myurl, 'pk':pk})



