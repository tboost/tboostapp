from django.shortcuts import render, get_object_or_404, redirect
from .models import UserProfile
from manageuser.models import ManageUser
from news.models import News
from main.models import Main
from cat.models import Cat
from django.utils import timezone
from subcat.models import Subcat
from trending.models import Trending
from django.contrib.auth import authenticate, login, logout
from django.core.files.storage import FileSystemStorage
import random
from random import randint
from django.contrib.auth.models import User, Group, Permission
from django.contrib.contenttypes.models import ContentType
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage



#Your function here...
def user_profile(request):

    #check login
    if not request.user.is_authenticated:
        return redirect('mylogin')

    #user access check start
    perm = 0
    for i in request.user.groups.all():
        if i.name == "masteruser": perm = 1
    if perm == 0:
        error = "Access Denied!"
        return render(request, 'back/error.html', {'error':error})
    
    if request.method == "POST":
        user = request.POST.get('user')
        description = request.POST.get('description')
        city = request.POST.get('city')
        website = request.POST.get('website')
        phone = request.POST.get('phone')

        #check empty field
        if user == "" or description == "" or city == "" or website == "" or phone == "":
            error = "Empty field not required"
            return redirect(request, 'back/error.html', {'error':error})
        
        db = UserProfile(user=request.user(), description=description, city=city, website=website, phone=phone)
        db.save()
    
    return render(request, 'back/profile_add.html')