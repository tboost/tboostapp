from django.conf.urls import url
from . import views

urlpatterns = [

    url(r'^contact/add/$', views.contact_add, name="contact_add"),
    url(r'^panel/contact/show/$', views.contact_show, name="contact_show"),
    url(r'^panel/contact/del/(?P<pk>\d+)/$', views.contact_del, name='contact_del'),
    url(r'^panel/contact/reply/(?P<pk>\d+)/$', views.reply_cm, name='reply_cm'),
    
    
    


]