from django.shortcuts import render, get_object_or_404, redirect
from .models import ContactForm
from main.models import Main
from django.core.files.storage import FileSystemStorage
from django.utils import timezone
from subcat.models import Subcat
from cat.models import Cat
from django.core.mail import send_mail
from django.conf import settings



#function to add contact
def contact_add(request):

    if request.method == 'POST':
        name = request.POST.get('name')
        mymail = request.POST.get('email')
        txt = request.POST.get('msg')

        #check empty fields
        if name == "" or mymail == "" or txt == "" :
            msg = "**All fields required!**"

            return render(request, 'back/msgbox.html', {'msg':msg})

        #check for duplicate email address
        # if mymail != 0:
        #     msg = f"email already exist!, {mymail}"
        #     return render(request, 'back/msgbox.html', {'msg':msg})

        #save to database
        db = ContactForm(name=name, mymail=mymail, txt=txt, date=timezone.now())
        db.save()
        
        msg = "Your Message Recieved with Thanks!"      


    return render(request, 'back/msgbox.html', {'msg':msg})

#contact list function
def contact_show(request):

    #check login start
    if not request.user.is_authenticated:
        return redirect('mylogin')
    #check login ends

    msg = ContactForm.objects.all()

    return render(request, 'back/contact_list.html', {'msg':msg})

def contact_del(request, pk):

    #check user login start
    if not request.user.is_authenticated:
        return redirect('mylogin')
    #check user login end

    m = ContactForm.objects.filter(pk=pk)
    m.delete()

    return redirect('contact_show')

#reply function
def reply_cm(request,pk):

    if request.method == 'POST':
        txt = request.POST.get('txt')

        #check for empty field
        if txt == "":
            error = "Empty message not allowed!"
            return render(request, 'back/error.html', {'error':error})
        
        #email settings
        to_email = ContactForm.objects.get(pk=pk).mymail
        subject = 'message reply'
        message = txt
        email_from = settings.EMAIL_HOST_USER
        emails = [to_email]
        send_mail(subject,message,email_from,emails)

        '''
        #You can also configure your sent mail also like this but you need to be on the host server
        send_mail(
            'mail reply',
            txt,
            'sender@tboost.com.ng',
            [to_email],
            fail_silently=False,
        )
        '''

    return render(request, 'back/reply_cm.html', {'pk':pk})
